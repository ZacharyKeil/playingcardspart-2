// Playing Cards
// Sawyer Grambihler
//Zachary Keil

#include <iostream>
#include <conio.h>
#include <cstdio>


using namespace std;

enum  Suit {
	Diamonds,
	Hearts,
	Clubs,
	Spades
};

enum  Rank {
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Suit Suit;
	Rank Rank;
};



void PrintCard(Card card) {
    string rankText = "", suitText = "";
    

    switch (card.Rank) 
    {           //making switch case that checks for card rank and sets output to designated char
    case Two: rankText = "2";
        break;
    case Three: rankText = "3";
        break;
    case Four: rankText = "4";
        break;
    case Five: rankText = "5";
        break;
    case Six: rankText = "6";
        break;
    case Seven: rankText = "7";
        break;
    case Eight: rankText = "8";
        break;
    case Nine: rankText = "9";
        break;
    case Ten: rankText = "10";
        break;
    case King: rankText = "King";
        break;
    case Queen: rankText = "Queen";
        break;
    case Jack: rankText = "Jack";
        break;
    case Ace: rankText = "Ace"; 
        break;
    }
    
    switch (card.Suit) 
    {                   //switch case to check for car suit and sets designated suit as the approiate  char
    case Clubs: suitText = "Clubs"; 
        break;
    case Diamonds: suitText = "Diamonds"; 
        break;
    case Hearts: suitText = "Hearts"; 
        break;
    case Spades: suitText = "Spades"; 
        break;
    }
    
    cout << rankText << " of " << suitText << endl; //outs card rank and text to console
}

Card HighCard(Card card1, Card card2) { 
    if (card1.Rank > card2.Rank) {
        return card1;
    }
    else if (card1.Rank < card2.Rank) {
        return card2;
    }
    else {
        
        if (card1.Suit > card2.Suit) {
            return card1;
        }
        else {
            return card2;
        }
    }
}

int main() {

    Card c1, c2;
    c1.Rank = Five;
    c1.Suit = Hearts;
    c2.Rank = Jack;
    c2.Suit = Spades;
  
   // cout << "Card1: ";
  // PrintCard(c1);
    //cout << "Card2: "; //i dont know of both did not want to print to console or if you wanted just the HighCard function not to print out.
   // PrintCard(c2);
    

    std::getchar();
    
    return 0;

}
